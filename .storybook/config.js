import {addDecorator, configure} from '@storybook/react';
import {themes} from '@storybook/components';
import {withThemesProvider} from 'storybook-addon-styled-component-theme';
import theme from "../src/theme";

// styled components
addDecorator(withThemesProvider([theme]));

// Option defaults.

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
