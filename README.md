## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

#### `npm run storybook`

Runs the app ui storybook.

### `npm run start-mock-server`

Runs the app mock server in the development mode.

## Structure
- **.env** This file contains all the configuration used inside the app
- **/src** All source code 
  - **index.js** Application entry file
  - **/theme** All application colors, sizes defined here
  - **/lib** General helpers functions 
  - **/images** Imported images
  - **/components** All components (JSX)
    - **App.js** Main app component
    - **/styled** Design only components (dump)
    - **/connected** Business related components (templates)
    - **/routes** Routes components
  - **/redux** All data logic (redux, saga)
      - **api.js** All api calls
      - **configureStore,js** Configure redux store 
      - **history.js** Shared redux history
      - **middleware.js** all redux middleware
      - **initialState.js** initial redux state (from all sub states)
      - **reducers.js** initial redux reducers (from all sub reducers)
      - **sagas.js** all side effects (using redux-saga)
      - `<Note: every directory here act as a redux state, to create a new state just copy one of the folders and edit it>`
        - **actions.js** state actions
        - **actionsTypes.js** state action types (enum)
        - **initial.js** initial state (default value)
        - **reducer.js** state reducers logic (pure functions)


## Notes
I have build everything almost from scratch including responsive design and ui components.
