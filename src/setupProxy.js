const proxy = require('proxy-middleware');
const {getEnv} = require("./lib/env");

module.exports = app => {
  // setup proxies
  app.use(getEnv("REACT_APP_API_PROXY"), proxy(getEnv("REACT_APP_API_BASE_URL")));
};
