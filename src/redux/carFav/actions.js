import actionTypes from "./actionTypes";

/**
 * add car to fav
 * @return {{type: String, payload: {car:*}}}
 */
export const addToFav = ({car}) => ({
  type: actionTypes.ADD_TO_FAV,
  payload: {
    car
  }
});

/**
 * remove car from fav
 * @return {{type: String, payload: {stockNumber:*}}}
 */
export const removeFromFav = ({stockNumber}) => ({
  type: actionTypes.REMOVE_FROM_FAV,
  payload: {
    stockNumber
  }
});

/**
 * clear car fav
 * @return {{type: String, payload: null}}
 */
export const clearFav = () => ({
  type: actionTypes.CLEAR_FAV
});
