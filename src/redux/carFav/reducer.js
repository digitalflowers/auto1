import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.ADD_TO_FAV: {
      const {car} = action.payload;
      return car ? state
        .update("fav", fav => fav
          .filter(({stockNumber}) => stockNumber !== car.stockNumber)
          .unshift(car)
        ) : state;
    }

    case actionTypes.REMOVE_FROM_FAV: {
      const {stockNumber} = action.payload;
      return stockNumber ? state
        .update("fav", fav => fav
          .filter(car => car.stockNumber !== stockNumber)
        ) : state;
    }

    case actionTypes.CLEAR_FAV:
      return state.set("fav", initialState.fav);

    default:
      return state;
  }
};
