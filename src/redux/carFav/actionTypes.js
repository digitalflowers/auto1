import {reaction} from "dacho";

let types = {
  ADD_TO_FAV: null,
  REMOVE_FROM_FAV: null,
  CLEAR_FAV: null
};

types = reaction(types, "CAR_FAV/");

export default types;
