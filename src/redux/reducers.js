import {combineReducers} from "redux";
import {createMigrate, createTransform, persistReducer} from "redux-persist";
import {connectRouter} from 'connected-react-router'
import storage from "redux-persist/lib/storage";
import initialState from "./initialState";
import car from "./car/reducer";
import cars from "./cars/reducer";
import carsFilters from "./carsFilters/reducer";
import carFav from "./carFav/reducer";
import history from "./history";
import {mergeDeepRecord} from "../lib/immutable";

// our reducers
const reducers = combineReducers({
  car,
  cars,
  carsFilters,
  carFav,
  router: connectRouter(history)
});

/**
 * redux persist reducer
 */
export default persistReducer({
  key: 'auto1',
  version: 1,
  storage, // defaults to localStorage for web and AsyncStorage for react-native
  blacklist: ['router'],
  migrate: createMigrate({
    0: () => initialState
  }, {debug: true}),
  transforms: [createTransform(
    // transform state on its way to being serialized and persisted.
    (inboundState, key) => initialState[key] && initialState[key].toJS ? inboundState.toJS() : inboundState,
    // transform state being rehydrated
    (outboundState, key) => initialState[key] && initialState[key].toJS ? mergeDeepRecord(initialState[key], outboundState) : outboundState
  )]
}, reducers);
