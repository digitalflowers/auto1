import {createStore} from "redux";
import {persistStore} from 'redux-persist';
import persistedReducer from "./reducers";
import middleware, {sagaMiddleware} from "./middleware";
import rootSaga from "./sagas";

export const configureStore = onComplete => {
  const store = createStore(persistedReducer, middleware);
  persistStore(store, null, onComplete);
  // run the saga
  sagaMiddleware.run(rootSaga);
  return store;
};
