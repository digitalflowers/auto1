import api from "../lib/api";
import {recordFromJS} from "../lib/immutable";

/**
 * fetch car
 * @param stockNumber
 * @return {*}
 */
export const fetchCar = ({stockNumber}) => api
  .get(`/cars/${stockNumber}`)
  .then(({data: {car}}) => recordFromJS({car}));

/**
 * fetch cars
 * @param manufacturer
 * @param color
 * @param sort
 * @param page
 * @return {*}
 */
export const fetchCars = ({manufacturer, color, sort, page}) => api
  .get("/cars", {
    params: {
      manufacturer,
      color,
      sort,
      page
    }
  })
  .then(({data: {cars, totalPageCount}}) => recordFromJS({cars, totalPageCount}));

/**
 * fetch cars colors
 * @return {*}
 */
export const fetchColors = () => api
  .get("/colors")
  .then(({data: {colors}}) => recordFromJS(colors));


/**
 * fetch cars manufacturers
 * @return {*}
 */
export const fetchManufacturers = () => api
  .get("/manufacturers")
  .then(({data: {manufacturers}}) => recordFromJS(manufacturers));
