import {all, call, put, takeLatest} from 'redux-saga/effects';
import * as api from './api';
import * as carActions from "./car/actions";
import * as carsActions from "./cars/actions";
import * as carsFiltersActions from "./carsFilters/actions";
import carActionTypes from "./car/actionTypes";
import carsActionTypes from "./cars/actionTypes";
import carsFiltersActionTypes from "./carsFilters/actionTypes";


/**
 * fetch car saga
 * @param action
 * @return {IterableIterator<*>}
 */
function* fetchCarSaga(action) {
  try {
    const {car} = yield call(api.fetchCar, action.payload);
    yield put(carActions.fetchCarSuccess({car}));
  } catch (error) {
    yield put(carActions.fetchCarFailed({error}));
  }
}

/**
 * fetch cars saga
 * @param action
 * @return {IterableIterator<*>}
 */
function* fetchCarsSaga(action) {
  try {
    const {cars, totalPageCount} = yield call(api.fetchCars, action.payload);
    yield put(carsActions.fetchCarsSuccess({cars, totalPageCount}));
  } catch (error) {
    yield put(carsActions.fetchCarsFailed({error}));
  }
}


/**
 * fetch cars filters saga
 * @param action
 * @return {IterableIterator<*>}
 */
function* fetchCarsFiltersSaga(action) {
  try {
    const [colors, manufacturers] = yield all([api.fetchColors(), api.fetchManufacturers()]);
    yield put(carsFiltersActions.fetchCarsFiltersSuccess({colors, manufacturers}));
  } catch (error) {
    yield put(carsFiltersActions.fetchCarsFiltersFailed({error}));
  }
}


function* rootSaga() {
  // get only latest => disable concurrent
  yield takeLatest(carsActionTypes.FETCH_CARS, fetchCarsSaga);
  // get only latest => disable concurrent
  yield takeLatest(carActionTypes.FETCH_CAR, fetchCarSaga);
  // get only latest => disable concurrent
  yield takeLatest(carsFiltersActionTypes.FETCH_CARS_FILTERS, fetchCarsFiltersSaga);
}

export default rootSaga;
