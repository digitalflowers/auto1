import actionTypes from "./actionTypes";

/**
 * fetch car
 * @param manufacturer
 * @return {{type: String, payload: {stockNumber:*}}}
 */
export const fetchCar = ({stockNumber}) => ({
  type: actionTypes.FETCH_CAR,
  payload: {
    stockNumber
  }
});

/**
 * set car
 * @param car
 * @return {{type: String, payload: {car: *}}}
 */
export const fetchCarSuccess = ({car}) => ({
  type: actionTypes.FETCH_CAR_SUCCESS,
  payload: {
    car
  }
});

/**
 * fetch cars failed
 * @param error
 * @return {{type: String, payload: {error: *}}}
 */
export const fetchCarFailed = ({error}) => ({
  type: actionTypes.FETCH_CAR_FAILED,
  payload: {
    error
  }
});
