import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.FETCH_CAR:
      return state
        .set("pending", true)
        .set("error", null);

    case actionTypes.FETCH_CAR_SUCCESS:
      const {car} = action.payload;
      return state
        .set("pending", false)
        .set("car", car);

    case actionTypes.FETCH_CAR_FAILED:
      return state
        .set("pending", false)
        .set("error", action.payload.error);

    default:
      return state;
  }
};
