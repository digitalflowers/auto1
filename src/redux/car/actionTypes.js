import {reaction} from "dacho";

let types = {
  FETCH_CAR: null,
  FETCH_CAR_SUCCESS: null,
  FETCH_CAR_FAILED: null
};

types = reaction(types, "CAR/");

export default types;
