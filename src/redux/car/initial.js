import {recordFromJS} from "../../lib/immutable";

export default recordFromJS({
  car: null,
  error: null,
  pending: false
});
