import {recordFromJS} from "../../lib/immutable";

export default recordFromJS({
  colors: [],
  manufacturers: [],
  error: null,
  pending: false
});
