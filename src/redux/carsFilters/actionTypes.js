import {reaction} from "dacho";

let types = {
  FETCH_CARS_FILTERS: null,
  FETCH_CARS_FILTERS_SUCCESS: null,
  FETCH_CARS_FILTERS_FAILED: null
};

types = reaction(types, "CARS_FILTERS/");

export default types;
