import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.FETCH_CARS_FILTERS:
      return state
        .set("pending", true)
        .set("error", null);

    case actionTypes.FETCH_CARS_FILTERS_SUCCESS:
      const {colors, manufacturers} = action.payload;
      return state
        .set("pending", false)
        .set("colors", colors)
        .set("manufacturers", manufacturers);

    case actionTypes.FETCH_CARS_FILTERS_FAILED:
      return state
        .set("pending", false)
        .set("error", action.payload.error);

    default:
      return state;
  }
};
