import actionTypes from "./actionTypes";

/**
 * fetch cars filters
 * @return {{type: String}}
 */
export const fetchCarsFilters = () => ({
  type: actionTypes.FETCH_CARS_FILTERS
});

/**
 * fetch cars success
 * @param cars
 * @return {{type: String, payload: {colors: *, manufacturers: *}}}
 */
export const fetchCarsFiltersSuccess = ({colors, manufacturers}) => ({
  type: actionTypes.FETCH_CARS_FILTERS_SUCCESS,
  payload: {
    colors,
    manufacturers
  }
});

/**
 * fetch cars failed
 * @param error
 * @return {{type: String, payload: {error: *}}}
 */
export const fetchCarsFiltersFailed = ({error}) => ({
  type: actionTypes.FETCH_CARS_FILTERS_FAILED,
  payload: {
    error
  }
});
