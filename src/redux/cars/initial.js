import {recordFromJS} from "../../lib/immutable";

export default recordFromJS({
  cars: [],
  totalPageCount: 1,
  filters: {
    manufacturer: null,
    color: null,
    sort: null,
    page: 1
  },
  error: null,
  pending: false
});
