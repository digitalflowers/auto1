import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.FETCH_CARS:
      return state
        .set("pending", true)
        .set("error", null)
        .update("filters", filters => filters.merge(action.payload));

    case actionTypes.FETCH_CARS_SUCCESS:
      const {cars, totalPageCount} = action.payload;
      return state
        .set("pending", false)
        .set("cars", cars)
        .set("totalPageCount", totalPageCount);

    case actionTypes.FETCH_CARS_FAILED:
      return state
        .set("pending", false)
        .set("error", action.payload.error);

    default:
      return state;
  }
};
