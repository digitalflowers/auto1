import {reaction} from "dacho";

let types = {
  FETCH_CARS: null,
  FETCH_CARS_SUCCESS: null,
  FETCH_CARS_FAILED: null
};

types = reaction(types, "CARS/");

export default types;
