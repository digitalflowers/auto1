import actionTypes from "./actionTypes";

/**
 * fetch cars
 * @param manufacturer
 * @param color
 * @param sort
 * @param page
 * @param totalPageCount
 * @return {{type: String, payload: {manufacturer: *, color: *, sort: *, page: *, totalPageCount: *}}}
 */
export const fetchCars = ({manufacturer, color, sort, page}) => ({
  type: actionTypes.FETCH_CARS,
  payload: {
    manufacturer: manufacturer === "none" ? null : manufacturer,
    color: color === "none" ? null : color,
    sort: sort === "none" ? null : sort,
    page: page || 1
  }
});

/**
 * set cars info
 * @param cars
 * @param manufacturer
 * @param totalPageCount
 * @return {{type: String, payload: {cars: *, totalPageCount: *}}}
 */
export const fetchCarsSuccess = ({cars, totalPageCount}) => ({
  type: actionTypes.FETCH_CARS_SUCCESS,
  payload: {
    cars,
    totalPageCount
  }
});

/**
 * fetch cars failed
 * @param error
 * @return {{type: String, payload: {error: *}}}
 */
export const fetchCarsFailed = ({error}) => ({
  type: actionTypes.FETCH_CARS_FAILED,
  payload: {
    error
  }
});
