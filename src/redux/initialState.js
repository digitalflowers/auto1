import car from "./car/initial";
import cars from "./cars/initial";
import carsFilters from "./carsFilters/initial";
import carFav from "./carFav/initial";

export default {
  car,
  cars,
  carsFilters,
  carFav
}
