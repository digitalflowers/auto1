import {applyMiddleware} from "redux";
import createSagaMiddleware from 'redux-saga'
import {createLogger} from "redux-logger";
import {routerMiddleware} from 'connected-react-router'
import history from "./history";
import {errorMiddleware} from "../lib/promises";
import {getEnv} from "../lib/env";

export const sagaMiddleware = createSagaMiddleware();

// check if enable logger middleware
const isLoggerEnabled = getEnv("REDUX_LOGGER_ENABLED") === "true";

const middleware = [
  routerMiddleware(history),
  sagaMiddleware,
  errorMiddleware
];
isLoggerEnabled && middleware.push(createLogger());

export default applyMiddleware(...middleware);
