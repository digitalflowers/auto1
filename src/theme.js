export default {
  // colors
  COLOR_PRIMARY: "#EA7F28",
  COLOR_ACCENT: "#D37324",
  COLOR_DARK: "#4A4A4A",
  COLOR_GRAY: "#EDEDED",
  // font weight
  FONT_WEIGHT_BOLD: "700",
  FONT_WEIGHT_NORMAL: "400",
  FONT_WEIGHT_LIGHT: "300",
  // font family
  FONT_FAMILY_DEFAULT: "'Roboto', sans-serif",
  // font size
  FONT_SIZE_S: "12px",
  FONT_SIZE_M: "14px",
  FONT_SIZE_L: "18px",
  FONT_SIZE_XL: "32px",
  // spacing
  SPACING_0: "0px",
  SPACING_S: "8px",
  SPACING_M: "12px",
  SPACING_L: "24px",
  // sizes
  SIZE_HEADER_HEIGHT: "80px",
  SIZE_FOOTER_HEIGHT: "80px",
  // media query break point
  MEDIA_MOBILE: "992px"
};
