import axios from "axios";
import {getEnv} from "./env";

const api = axios.create({
  "baseURL": getEnv("REACT_APP_API_PROXY") || getEnv("API_BASE_URL"),
  "timeout": parseInt(getEnv("API_TIMEOUT")),
  "headers": {
    "X-Requested-With": "XMLHttpRequest"
  }
});

export default api;
