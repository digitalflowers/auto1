/**
 * a better way to get a value from process.env object
 * @param name
 * @param defaultValue
 * @return {string | undefined | *}
 */
module.exports = {
  getEnv: (name, defaultValue) => process.env[name] || process.env[`REACT_APP_${name}`] || defaultValue
};
