import React from 'react';
import styled from 'styled-components';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {Text} from "../components/styled/text/Text";
import {Error} from "../components/styled/text/Error";
import {H1, H2} from "../components/styled/text/H";
import {Button} from "../components/styled/elements/Button";
import {Dropdown} from "../components/styled/elements/Dropdown";
import {Preloader} from "../components/styled/others/Preloader";
import {Link} from "../components/styled/elements/Link";
import {Menu} from "../components/styled/elements/Menu";
import {NavView} from "../components/styled/views/NavView";
import {NavBar, NavBarItem} from "../components/styled/elements/NavBar";
import {Banner} from "../components/styled/others/Banner";
import {Pager} from "../components/styled/others/Pager";
import {ColorMark} from "../components/styled/others/ColorMark";

const StoryPreview = styled.div`
  min-height: calc(100vh - 16px);
  font-family: 'Roboto', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

storiesOf('Text', module)
  .add('default', () => (
    <StoryPreview>
      <Text>Sample Text</Text>
    </StoryPreview>
  ))
  .add('error', () => (
    <StoryPreview>
      <Error title="Some Error">
        An error occurred while doing something.
      </Error>
    </StoryPreview>
  ))
  .add('H1', () => (
    <StoryPreview>
      <H1>
        Header 1
      </H1>
    </StoryPreview>
  ))
  .add('H2', () => (
    <StoryPreview>
      <H2>
        Header 2
      </H2>
    </StoryPreview>
  ))
  .add('Subtitle', () => (
    <StoryPreview>
      <H2>
        Subtitle
      </H2>
    </StoryPreview>
  ));

storiesOf('Link', module)
  .add('default', () => (
    <StoryPreview>
      <Link onClick={action("Clicked :)")}>
        Click Me
      </Link>
    </StoryPreview>
  ));

storiesOf('Button', module)
  .add('default', () => (
    <StoryPreview>
      <Button onClick={action("Clicked :)")}>
        Click Me
      </Button>
    </StoryPreview>
  ));

storiesOf('Dropdown', module)
  .add('default', () => (
    <StoryPreview>
      <div style={{maxWidth: "100px"}}>
        <Dropdown value={1} onChange={value => action(`${value} is Selected`)()} items={[{
          value: 1,
          text: "Option 1"
        }, {
          value: 2,
          text: "Option 2"
        }, {
          value: 3,
          text: "Option 3"
        }]}/>
      </div>
    </StoryPreview>
  ));

storiesOf('Menu', module)
  .add('default', () => (
    <StoryPreview>
      <div style={{maxWidth: "70px", margin: "0 auto"}}>
        <Menu icon={"bars"} items={[{
          to: "/",
          text: "Home",
          exact: true
        }, {
          to: "/favourites",
          text: "My Favourites",
          exact: false
        }, {
          to: "/Purchase",
          text: "Purchase",
          exact: false
        }, {
          to: "/sell",
          text: "Sell",
          exact: false
        }]}/>
      </div>
    </StoryPreview>
  ));

storiesOf('Navbar', module)
  .add('default', () => (
    <StoryPreview>
      <NavView>
        <NavBar>
          {[{
            to: "/",
            text: "Home",
            exact: true
          }, {
            to: "/favourites",
            text: "My Favourites",
            exact: false
          }, {
            to: "/Purchase",
            text: "Purchase",
            exact: false
          }, {
            to: "/sell",
            text: "Sell",
            exact: false
          }].map(({text, to, exact}) => (
            <NavBarItem key={to}>
              <Link to={to} exact={exact}>
                {text}
              </Link>
            </NavBarItem>
          ))}
        </NavBar>
      </NavView>
    </StoryPreview>
  ));

storiesOf('Pager', module)
  .add('default', () => (
    <StoryPreview p={0}>
      <Pager current={4} total={9} onSelect={index => action(`Page ${index} is Selected`)()}/>

    </StoryPreview>
  ));

storiesOf('Preloader', module)
  .add('default', () => (
    <StoryPreview>
      <Preloader/>
    </StoryPreview>
  ))
  .add('larger L', () => (
    <StoryPreview>
      <Preloader size="L"/>
    </StoryPreview>
  ))
  .add('custom color', () => (
    <StoryPreview>
      <Preloader size="L" color="red"/>
    </StoryPreview>
  ));

storiesOf('ColorMark', module)
  .add('default', () => (
    <StoryPreview p={0}>
      <ColorMark color="red"/>
    </StoryPreview>
  ));


storiesOf('Banner', module)
  .add('default', () => (
    <StoryPreview p={0}>
      <Banner imageSrc={"https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"}
              height="300px"/>
    </StoryPreview>
  ));
