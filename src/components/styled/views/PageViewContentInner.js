import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * page main content view
 */
export const PageViewContentInner = styled.div`
  margin: 0 auto;
  width: 100%;
  padding: ${props => props.theme.SPACING_L};
  max-width: ${props => props.maxWidth};
`;

PageViewContentInner.propTypes = {
  maxWidth: PropTypes.any
};

PageViewContentInner.defaultProps = {
  maxWidth: "none"
};
