import styled from "styled-components";

/**
 * center view, center content across x,y axis
 */
export const CenterView = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
