import styled from "styled-components";

/**
 * page view
 */
export const PageView = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  align-items: stretch;
  max-width: 100%;
  overflow: hidden;
  font-family: ${props => props.theme.FONT_FAMILY_DEFAULT};
  font-size: ${props => props.theme.FONT_SIZE_M};
  font-weight: ${props => props.theme.FONT_WEIGHT_NORMAL};
`;
