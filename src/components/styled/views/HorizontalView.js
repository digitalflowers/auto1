import styled, {css} from "styled-components";
import PropTypes from "prop-types";

/**
 * padding sizes
 * @type {string[]}
 */
const sizes = ["L", "M", "S"];

/**
 * horizontal view, across x axis
 */
export const HorizontalView = styled.div`
  display: flex;
  flex-direction: row;
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  flex: ${props => props.flex};
  // if box
  border-color: ${props => props.theme.COLOR_GRAY};
  border-style: ${props => props.box ? "solid" : "none"};
  border-width: ${props => props.box ? "2px" : "0"};
  padding: ${props => props.box ? props.theme[`SPACING_${props.box}`] || "0" : "0"};
  ${props => props.responsive ? css`
      @media (max-width: ${props => props.theme.MEDIA_MOBILE}) {
         flex-direction: column;
      }
  ` : ""}
   ${props => props.responsive && props.centerMobile ? css`
      @media (max-width: ${props => props.theme.MEDIA_MOBILE}) {
         & > * {
           align-self: center !important;
           text-align: center; 
           h1, h2 {
             text-align: center;
           }
         }
      }
  ` : ""}
`;

HorizontalView.propTypes = {
  alignItems: PropTypes.string,
  justifyContent: PropTypes.string,
  flex: PropTypes.any,
  box: PropTypes.oneOf(sizes),
  responsive: PropTypes.bool,
  centerMobile: PropTypes.bool
};
