import PropTypes from "prop-types";
import styled from "styled-components";

/**
 * page view footer
 */
export const PageViewFooter = styled.footer`
   display: flex;
   flex-direction: row;
   align-items: center;
   justify-content: center;
   color: ${props => props.theme.COLOR_DARK};
   font-size: ${props => props.theme.FONT_SIZE_M};
   border-top: 2px solid ${props => props.theme.COLOR_GRAY};
   height: ${props => props.height || props.theme.SIZE_FOOTER_HEIGHT};
`;
PageViewFooter.propTypes = {
  height: PropTypes.string
};
