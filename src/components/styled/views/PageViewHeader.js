import PropTypes from "prop-types";
import styled from "styled-components";

/**
 * page view header
 */
export const PageViewHeader = styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  font-size: ${props => props.theme.FONT_SIZE_L};
  padding-left: ${props => props.theme.SPACING_L};
  padding-right: ${props => props.theme.SPACING_L};
  border-bottom: 2px solid ${props => props.theme.COLOR_GRAY};
  height: ${props => props.height || props.theme.SIZE_HEADER_HEIGHT};
`;
PageViewHeader.propTypes = {
  height: PropTypes.string
};
