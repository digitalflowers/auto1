import styled from "styled-components";

/**
 * nav view
 */
export const NavView = styled.nav`
  .nav-mobile {
     display: none;
  }
  @media (max-width: ${props => props.theme.MEDIA_MOBILE}) {
    .nav {
       display: none;
    }
    .nav-mobile {
       display: block;
    }
  }
`;
