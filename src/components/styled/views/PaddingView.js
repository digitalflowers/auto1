import styled, {css} from "styled-components";
import PropTypes from "prop-types";

/**
 * padding sizes
 * @type {string[]}
 */
const sizes = ["0", "L", "M", "S"];

/**
 * padding view
 */
export const PaddingView = styled.div`
  flex: ${props => props.flex};
  align-self: ${props => props.alignSelf};
  max-width: ${props => props.maxWidth || "initial"};
  margin: ${props => props.margin || "0"};
  padding-left: ${props => props.left ? props.theme[`SPACING_${props.left}`] : "0"};
  padding-right: ${props => props.right ? props.theme[`SPACING_${props.right}`] : "0"};
  padding-top: ${props => props.top ? props.theme[`SPACING_${props.top}`] : "0"};
  padding-bottom: ${props => props.bottom ? props.theme[`SPACING_${props.bottom}`] : "0"};  
  @media (max-width: ${props => props.theme.MEDIA_MOBILE}) {
    ${props => props.leftMobile ? css`
        padding-left: ${props => props.theme[`SPACING_${props.leftMobile}`]} ;
    ` : ""}
    ${props => props.rightMobile ? css`
        padding-right: ${props => props.theme[`SPACING_${props.rightMobile}`]} ;
    ` : ""}
    ${props => props.topMobile ? css`
        padding-top: ${props => props.theme[`SPACING_${props.topMobile}`]} ;
    ` : ""}
    ${props => props.bottomMobile ? css`
        padding-bottom: ${props => props.theme[`SPACING_${props.bottomMobile}`]} ;
    ` : ""}
  }
`;


PaddingView.propTypes = {
  flex: PropTypes.any,
  alignSelf: PropTypes.any,
  maxWidth: PropTypes.any,
  margin: PropTypes.any,
  left: PropTypes.oneOf(sizes),
  right: PropTypes.oneOf(sizes),
  top: PropTypes.oneOf(sizes),
  bottom: PropTypes.oneOf(sizes),
  leftMobile: PropTypes.oneOf(sizes),
  rightMobile: PropTypes.oneOf(sizes),
  topMobile: PropTypes.oneOf(sizes),
  bottomMobile: PropTypes.oneOf(sizes),
};
