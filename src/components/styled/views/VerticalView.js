import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * padding sizes
 * @type {string[]}
 */
const sizes = ["L", "M", "S"];

/**
 * vertical view, across y axis
 */
export const VerticalView = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${props => props.alignItems};
  justify-content: ${props => props.justifyContent};
  flex: ${props => props.flex};
  // if box
  border-color: ${props => props.theme.COLOR_GRAY};
  border-style: ${props => props.box ? "solid" : "none"};
  border-width: ${props => props.box ? "2px" : "0"};
  padding: ${props => props.box ? props.theme[`SPACING_${props.box}`] || "0" : "0"};
`;

VerticalView.propTypes = {
  alignItems: PropTypes.string,
  justifyContent: PropTypes.string,
  flex: PropTypes.any,
  box: PropTypes.oneOf(sizes)
};
