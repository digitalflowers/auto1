import styled from "styled-components";

/**
 * page view content
 */
export const PageViewContent = styled.div`
  flex: 1;
  display: flex;
`;
