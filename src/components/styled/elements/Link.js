import styled from "styled-components";

/**
 * button
 */
export const Link = styled.a`
  cursor: pointer;
  text-decoration: none;
  color: ${props => props.theme.COLOR_PRIMARY};
  transition: 0.2s color;
  &.active, &:hover, &:active, &:focus {
    text-decoration: underline;
    color: ${props => props.theme.COLOR_ACCENT};
  }
`;
