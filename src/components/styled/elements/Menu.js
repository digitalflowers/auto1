import React, {Component} from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import {NavLink} from "react-router-dom";
import {Link} from "./Link";

/**
 * main
 */
const Select = styled.div`
  position: relative; 
  line-height: 2.3;
  text-transform: capitalize;
  .label {
     padding-left: 2px;
     line-height: 1;
  }
  .selected {
    display: block;
    text-decoration: none;
    color: inherit;
    position: relative;
    border-radius: 4px;
    background-color: white;
    padding: ${props => props.theme.SPACING_S} ${props => props.theme.SPACING_L};
    border: 2px solid ${props => props.theme.COLOR_GRAY};
    .fa {
        font-size: ${props => props.theme.FONT_SIZE_L};
        color: ${props => props.theme.COLOR_PRIMARY};
    }
  }
  .select-wrapper {
      min-width: 250px;
      position: absolute;
      z-index: 999;
      top: 100%;
      right: 0;
      max-height: 300px;
      overflow-y: auto;
      margin: ${props => props.theme.SPACING_S} 0;
      border: 2px solid ${props => props.theme.COLOR_GRAY};
      .select-item {
         white-space: nowrap;
         text-overflow: ellipsis;
         overflow: hidden;
         text-decoration: none;
         color: inherit;
         display: block;
         background-color: white;
         padding-left: ${props => props.theme.SPACING_S};
         padding-right: ${props => props.theme.SPACING_S};
         border-bottom: 2px solid ${props => props.theme.COLOR_GRAY};
         transition: 0.2s background-color, 0.2s color;
         &.active, &:hover {
            background-color: ${props => props.theme.COLOR_PRIMARY};
            color: white;
         }
         &:last-child {
            border-bottom: none;
         }
      }
  }
`;

/**
 * menu
 */
export class Menu extends Component {
  node = null;

  state = {
    open: false
  };

  componentWillMount() {
    document.addEventListener("mousedown", this.onClose, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.onClose, false);
  }

  onToggle = evt => {
    this.setState({open: !this.state.open});
    evt.preventDefault();
    return false;
  };

  onClose = evt => {
    if (this.node && !this.node.contains(evt.target)) {
      this.close();
      evt.preventDefault();
      return false;
    }
  };

  close = () => this.setState({open: false});

  render() {
    const {label, items, icon, withRouter} = this.props;
    const {open} = this.state;
    return (
      <Select ref={node => this.node = node}>
        <a className="selected"
           href="#selected"
           onClick={this.onToggle}>
          <span>{label}</span>
          {icon ? (
            <i className={`fa fa-${icon}`}/>
          ) : null}
        </a>
        {open ? (
          <div className="select-wrapper">
            {items ? items.map(({text, to, exact}) =>
              <Link as={withRouter ? NavLink : "a"}
                    key={to}
                    exact={exact}
                    to={to}
                    className="select-item">
                <div onClick={() => setTimeout(this.close, 200)}>{text}</div>
              </Link>
            ) : null}
          </div>
        ) : null}
      </Select>
    );
  }
}

Menu.propTypes = {
  icon: PropTypes.string,
  label: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.any,
    to: PropTypes.any,
    exact: PropTypes.bool
  })),
  withRouter: PropTypes.bool
};

Menu.defaultProps = {
  items: [],
  withRouter: false
};
