import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * nav bar
 */
export const NavBar = styled.ul`
  display: flex;
  list-style: none;
  flex-direction: row;
  text-align: ${props => props.textAlign};
`;

NavBar.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"])
};

NavBar.defaultProps = {
  alignItems: "center",
  className: "nav"
};

/**
 * nav bar
 */
export const NavBarMobile = styled.ul`
  display: flex;
  list-style: none;
  flex-direction: row;
  text-align: ${props => props.textAlign};
`;

NavBarMobile.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"])
};

NavBarMobile.defaultProps = {
  alignItems: "center",
  className: "nav nav-mobile"
};

/**
 * nav bar item
 */
export const NavBarItem = styled.li`
  padding: ${props => props.padding || props.theme.SPACING_M};
  display: inline-block;
  a {
    color: ${props => props.theme.COLOR_DARK};
    &:hover, &:focus, &.active {
      color: ${props => props.theme.COLOR_PRIMARY};
      text-decoration: none;
    }
  }
`;
