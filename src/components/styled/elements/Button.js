import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * button
 */
export const Button = styled.button`
  text-align: center;
  width: 128px;
  line-height: 32px;
  cursor: pointer;
  display: inline-block;
  text-decoration: none;
  transition: 0.2s background-color;
  color: ${props => props.primary ? "white" : props.theme.COLOR_PRIMARY};
  font-size: ${props => props.theme.FONT_SIZE_M};
  background-color: ${props => props.primary ? props.theme.COLOR_PRIMARY : "white"};
  border: 2px solid ${props => props.theme.COLOR_PRIMARY};
  :hover, :active, :focus {
    text-decoration: none;
    background-color: ${props => props.primary ? props.theme.COLOR_ACCENT : "white"};
    border: 2px solid ${props => props.theme.COLOR_ACCENT};
  }
`;

Button.propTypes = {
  primary: PropTypes.bool
};
