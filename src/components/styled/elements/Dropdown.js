import React, {Component} from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * main
 */
const Select = styled.div`
  position: relative; 
  min-width: 250px;
  line-height: 2.3;
  text-transform: capitalize;
  .label {
     padding-left: 2px;
     line-height: 1;
  }
  .selected {
    display: block;
    text-decoration: none;
    color: inherit;
    position: relative;
    border-radius: 4px;
    padding-right: 30px;
    background-color: white;
    padding-left: ${props => props.theme.SPACING_S};
    margin: ${props => props.theme.SPACING_S} 0;
    border: 2px solid ${props => props.theme.COLOR_GRAY};
    .fa {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        font-size: ${props => props.theme.FONT_SIZE_L};
        right: ${props => props.theme.SPACING_S};
        color: ${props => props.theme.COLOR_GRAY};
    }
  }
  .select-wrapper {
      position: absolute;
      z-index: 999;
      top: 100%;
      left: 0;
      right: 0;
      max-height: 300px;
      overflow-y: auto;
      margin: ${props => props.theme.SPACING_S} 0;
      border: 2px solid ${props => props.theme.COLOR_GRAY};
      .select-item {
         white-space: nowrap;
         text-overflow: ellipsis;
         overflow: hidden;
         text-decoration: none;
         color: inherit;
         display: block;
         background-color: white;
         padding-left: ${props => props.theme.SPACING_S};
         padding-right: ${props => props.theme.SPACING_S};
         border-bottom: 2px solid ${props => props.theme.COLOR_GRAY};
         transition: 0.2s background-color, 0.2s color;
         &.active, &:hover {
            background-color: ${props => props.theme.COLOR_PRIMARY};
            color: white;
         }
         &:last-child {
            border-bottom: none;
         }
      }
  }
`;

/**
 * dropdown menu
 */
export class Dropdown extends Component {
  node = null;

  state = {
    open: false,
    value: ""
  };

  constructor(props) {
    super(props);
    this.state.value = props.value;
  }

  componentWillMount() {
    document.addEventListener("mousedown", this.onClose, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.onClose, false);
  }

  onClick = value => evt => {
    this.onSelect(value || null);
    evt.preventDefault();
    return false;
  };

  onSelect = value => {
    const {onChange} = this.props;
    this.setState({open: false}, () => this.setState({value}, () => onChange && onChange(value)));
  };

  onToggle = evt => {
    this.setState({open: !this.state.open});
    evt.preventDefault();
    return false;
  };

  onClose = evt => {
    if (this.node && !this.node.contains(evt.target)) {
      this.setState({open: false});
      evt.preventDefault();
      return false;
    }
  };

  render() {
    const {label, items} = this.props;
    const {open, value} = this.state;
    const selected = items ? items.find(({value: _value}) => value === _value) : null;
    return (
      <Select ref={node => this.node = node}>
        <div className="label">
          {label}
        </div>
        <a className="selected"
           href="#selected"
           onClick={this.onToggle}>
          <span>{selected ? selected.text : "-"}</span>
          <i className={`fa fa-caret-${open ? "up" : "down"}`}/>
        </a>
        {open ? (
          <div className="select-wrapper">
            {items ? items.map(({text, value: _value}) =>
              <a key={_value}
                 href={`#${_value}`}
                 className={`select-item ${_value === value ? "active" : ""}`}
                 onClick={this.onClick(_value)}>
                {text}
              </a>
            ) : null}
          </div>
        ) : null}
      </Select>
    );
  }
}

Dropdown.propTypes = {
  value: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    text: PropTypes.any
  }))
};

Dropdown.defaultProps = {
  items: []
};
