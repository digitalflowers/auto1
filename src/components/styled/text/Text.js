import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * text
 */
export const Text = styled.p`
  margin: 0;
  line-height: 1.6;
  font-size: ${props => props.theme.FONT_SIZE_L};
  text-align: ${props => props.textAlign};
`;

Text.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"])
};

Text.defaultProps = {
  textAlign: "left"
};
