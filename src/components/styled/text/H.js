import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * header 1
 */
export const H1 = styled.h1`
  margin: 0;
  line-height: 2.6;
  font-weight: ${props => props.theme.FONT_WEIGHT_BOLD};
  font-size: ${props => props.theme.FONT_SIZE_XL};
  text-align: ${props => props.textAlign};
`;

H1.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"])
};

H1.defaultProps = {
  textAlign: "left"
};

export const H2 = styled.h2`
  margin: 0;
  font-weight: ${props => props.theme.FONT_WEIGHT_BOLD};
  font-size: ${props => props.theme.FONT_SIZE_L};
  text-align: ${props => props.textAlign};
  color: ${props => props.theme[props.color] || props.color};
`;

H2.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"]),
  color: PropTypes.any
};

H2.defaultProps = {
  textAlign: "left"
};
