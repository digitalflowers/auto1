import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * error
 */
const Text = styled.div`
  margin: 0;
  font-size: ${props => props.theme.FONT_SIZE_L};
  text-align: ${props => props.textAlign};
  .details, .fa {
    color: #cc0a00;
    text-align: ${props => props.textAlign};
    font-size: ${props => props.theme.FONT_SIZE_M};
  }
  .fa {
    padding: ${props => props.theme.SPACING_S};
  }
`;

export const Error = ({title, children, ...rest}) => (
  <Text {...rest}>
    <p>
      {title}
    </p>
    <p className="details">
      <i className="fa fa-exclamation-triangle"/> {children}
    </p>
  </Text>
);
Error.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"]),
  title: PropTypes.string
};

Error.defaultProps = {
  textAlign: "center",
  title: "Ops! something went wrong!"
};
