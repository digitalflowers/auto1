import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * subtitle
 */
export const Subtitle = styled.p`
  margin: 0;
  line-height: 2.2;
  font-size: ${props => props.theme.FONT_SIZE_L};
  text-align: ${props => props.textAlign};
`;

Subtitle.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"])
};

Subtitle.defaultProps = {
  textAlign: "left"
};
