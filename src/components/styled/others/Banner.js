import styled from "styled-components";
import PropTypes from "prop-types";

export const Banner = styled.div`
  width: 100%;
  background-size: auto;
  height: ${props => props.height};
  background: ${props => props.theme.COLOR_GRAY} url(${props => props.imageSrc}) no-repeat center center;
`;

Banner.propTypes = {
  imageSrc: PropTypes.string,
  height: PropTypes.string
};
