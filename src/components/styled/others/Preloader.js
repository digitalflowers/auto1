import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * mini preloader
 */
export const Preloader = styled.i.attrs({
  className: "fa fa-spinner fa-spin",
})`
  color: ${props => props.color || props.theme.COLOR_PRIMARY};
  font-size: ${props => props.theme[`SPACING_${props.size}`] || props.size};
`;

Preloader.propTypes = {
  size: PropTypes.any,
  color: PropTypes.any
};
