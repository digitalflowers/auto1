import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * filter color mark
 */
export const ColorMark = styled.span`
  display: inline-block;
  width: 10px;
  height: 10px;
  border-radius: 4px;
  vertical-align: middle;
  margin-right: 4px;
  text-transform: capitalize;
  background-color: ${props => props.color}; 
  border: 2px solid ${props => props.theme.COLOR_GRAY};
`;

ColorMark.propTypes = {
  color: PropTypes.string
};
