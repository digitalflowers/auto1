import React from "react";
import PropTypes from "prop-types";
import {PaddingView} from "../views/PaddingView";
import {HorizontalView} from "../views/HorizontalView";
import {Link} from "../elements/Link";

/**
 * pager
 * @return {*}
 * @constructor
 * @param current
 * @param total
 * @param nextTo
 * @param previousTo
 * @param lastTo
 * @param firstTo
 * @param onSelect
 */
export const Pager = ({current, total, firstTo, previousTo, nextTo, lastTo, onSelect}) => (
  <HorizontalView justifyContent="center">
    {current > 1 ? (
      <PaddingView right="M" left="M">
        <Link onClick={() => onSelect && onSelect(1)}>
          First
        </Link>
      </PaddingView>
    ) : null}
    {current > 1 ? (
      <PaddingView right="M" left="M">
        <Link onClick={() => onSelect && onSelect(Math.max(1, current - 1))}>
          Previous
        </Link>
      </PaddingView>
    ) : null}
    <PaddingView right="M" left="M">
      Page {current} from {total}
    </PaddingView>
    {current < total ? (
      <PaddingView right="M" left="M">
        <Link onClick={() => onSelect && onSelect(Math.min(total, current + 1))}>
          Next
        </Link>
      </PaddingView>
    ) : null}
    {current < total ? (
      <PaddingView right="M" left="M">
        <Link onClick={() => onSelect && onSelect(total)}>
          Last
        </Link>
      </PaddingView>
    ) : null}
  </HorizontalView>
);

Pager.propTypes = {
  current: PropTypes.number,
  total: PropTypes.number,
  onSelect: PropTypes.func
};
