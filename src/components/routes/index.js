import React, {Component} from "react";
import {Route, Switch} from "react-router";
import {NavLink} from "react-router-dom";
import logo from "../../images/logo.png";
import {PageView} from "../styled/views/PageView";
import {PageViewHeader} from "../styled/views/PageViewHeader";
import {PageViewContent} from "../styled/views/PageViewContent";
import {PageViewFooter} from "../styled/views/PageViewFooter";
import {NavView} from "../styled/views/NavView";
import {NavBar, NavBarItem, NavBarMobile} from "../styled/elements/NavBar";
import {Link} from "../styled/elements/Link";
import HomeRoute from "./home";
import CarRoute from "./car";
import NotFoundRoute from "./404";
import FavRoute from "./fav";
import {Menu} from "../styled/elements/Menu";

const navItems = [{
  to: "/",
  text: "Home",
  exact: true
}, {
  to: "/favourites",
  text: "My Favourites",
  exact: false
}, {
  to: "/Purchase",
  text: "Purchase",
  exact: false
}, {
  to: "/sell",
  text: "Sell",
  exact: false
}];

class Routes extends Component {
  render() {
    return (
      <PageView>
        <PageViewHeader>
          <Link as={NavLink} to="/">
            <img src={logo} width="180px" alt="logo"/>
          </Link>
          <NavView>
            <NavBarMobile>
              <NavBarItem padding="0">
                <Menu items={navItems} icon="bars"/>
              </NavBarItem>
            </NavBarMobile>
            <NavBar>
              {navItems.map(({text, to, exact}) => (
                <NavBarItem key={to}>
                  <Link as={NavLink} to={to} exact={exact}>
                    {text}
                  </Link>
                </NavBarItem>
              ))}
            </NavBar>
          </NavView>
        </PageViewHeader>
        <PageViewContent>
          <Switch>
            <Route path="/" exact component={HomeRoute}/>
            <Route path="/favourites" exact component={FavRoute}/>
            <Route path="/car/:stockNumber" component={CarRoute}/>
            <Route component={NotFoundRoute}/>
          </Switch>
        </PageViewContent>
        <PageViewFooter>
          &copy; AUTO1 Group 2018
        </PageViewFooter>
      </PageView>
    );
  }
}

export default Routes;
