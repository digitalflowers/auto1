import React, {Component} from "react";
import {PageViewContentInner} from "../styled/views/PageViewContentInner";
import CarsListFav from "../connected/CarsListFav";

class FavRoute extends Component {
  render() {
    return (
      <PageViewContentInner>
        <CarsListFav/>
      </PageViewContentInner>
    );
  }
}

export default FavRoute;
