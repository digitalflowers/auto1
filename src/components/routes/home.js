import React, {Component} from "react";
import {PageViewContentInner} from "../styled/views/PageViewContentInner";
import {HorizontalView} from "../styled/views/HorizontalView";
import {PaddingView} from "../styled/views/PaddingView";
import CarsFilters from "../connected/CarsFilters";
import CarsList from "../connected/CarsList";

class HomeRoute extends Component {
  render() {
    return (
      <PageViewContentInner>
        <HorizontalView responsive={true}>
          <PaddingView flex={3} bottom="L">
            <CarsFilters/>
          </PaddingView>
          <PaddingView flex={9} left="L" leftMobile="0">
            <CarsList/>
          </PaddingView>
        </HorizontalView>
      </PageViewContentInner>
    );
  }
}

export default HomeRoute;
