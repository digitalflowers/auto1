import React, {Component} from "react";
import {connect} from "react-redux";
import {Link as RouterLink} from 'react-router-dom';
import {fetchCar} from "../../redux/car/actions";
import {VerticalView} from "../styled/views/VerticalView";
import {Preloader} from "../styled/others/Preloader";
import {Button} from "../styled/elements/Button";
import {Error} from "../styled/text/Error";
import {PaddingView} from "../styled/views/PaddingView";
import {HorizontalView} from "../styled/views/HorizontalView";
import {Banner} from "../styled/others/Banner";
import {H1} from "../styled/text/H";
import {Subtitle} from "../styled/text/Subtitle";
import {ColorMark} from "../styled/others/ColorMark";
import {Text} from "../styled/text/Text";
import CarSave from "../connected/CarSave";

class CarRoute extends Component {
  componentWillMount() {
    const {match} = this.props;
    match
    && match.params
    && match.params.stockNumber
    && this.fetchCar({stockNumber: match.params.stockNumber});
  }

  componentDidUpdate({match: prevMatch}) {
    const {match} = this.props;
    match
    && match.params
    && match.params.stockNumber
    && (!prevMatch || !prevMatch.params || prevMatch.params.stockNumber !== match.params.stockNumber)
    && this.fetchCar({stockNumber: match.stockNumber});
  }

  fetchCar = ({stockNumber}) => {
    const {dispatch} = this.props;
    dispatch(fetchCar({stockNumber}));
  };

  render() {
    const {car, pending, error} = this.props;
    const {pictureUrl, manufacturerName, modelName, stockNumber, mileage, fuelType, color} = car || {};
    return (
      <VerticalView flex={1}>
        {
          pending || (!car && !error) ? (
            <VerticalView alignItems="center" justifyContent="center">
              <PaddingView top="L">
                <Preloader size="L"/>
              </PaddingView>
            </VerticalView>
          ) : (
            error ? (
              <VerticalView alignItems="center">
                <Error>
                  {error.message}
                </Error>
                <HorizontalView>
                  <PaddingView right="S">
                    <Button primary={true} onClick={this.fetchCar}>
                      Retry
                    </Button>
                  </PaddingView>
                  <PaddingView left="S">
                    <Button as={RouterLink} to="/">
                      Go Homepage
                    </Button>
                  </PaddingView>
                </HorizontalView>
              </VerticalView>
            ) : (
              <VerticalView>
                <Banner imageSrc={pictureUrl} height="300px"/>
                <PaddingView maxWidth="800px" margin="0 auto">
                  <HorizontalView responsive={true}>
                    <VerticalView flex={7}>
                      <PaddingView left="M" right="M">
                        <H1>
                          {manufacturerName} {modelName}
                        </H1>
                      </PaddingView>
                      <PaddingView left="M" right="M" bottom="L">
                        <Subtitle>
                          Stock
                          # {stockNumber} - {mileage.number} {mileage.unit.toUpperCase()} - {fuelType} - <ColorMark
                          color={color}/> {color}
                        </Subtitle>
                      </PaddingView>
                      <PaddingView left="M" right="M" bottom="L">
                        <Text>
                          This car is currently available and can be delivered as soon as tomorrow morning. Please be
                          aware that delivery times shown in this page are not definitive and may change due to bad
                          weather conditions.
                        </Text>
                      </PaddingView>
                    </VerticalView>
                    <PaddingView flex={5} left="L" top="L" bottom="L" rightMobile="L">
                      <CarSave/>
                    </PaddingView>
                  </HorizontalView>
                </PaddingView>
              </VerticalView>
            )
          )
        }
      </VerticalView>
    );
  }
}

export default connect((({car: {car, pending, error}}) => ({
  car,
  pending,
  error
})))(CarRoute);
