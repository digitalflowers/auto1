import React, {Component} from "react";
import {Link as RouterLink} from "react-router-dom";
import logo from "../../images/logo.png";
import {VerticalView} from "../styled/views/VerticalView";
import {H1} from "../styled/text/H";
import {Subtitle} from "../styled/text/Subtitle";
import {Link} from "../styled/elements/Link";

class NotFoundRoute extends Component {
  render() {
    return (
      <VerticalView alignItems="center" justifyContent="center" flex="1">
        <img src={logo} width="180px" alt="logo"/>
        <H1 textAlign="center">
          404 - Not Found
        </H1>
        <Subtitle textAlign="center" margin="0">
          Sorry, the page you are looking for does not exist.<br/>
          You can always go back to the <Link as={RouterLink} to="/">homepage</Link>.
        </Subtitle>
      </VerticalView>
    );
  }
}

export default NotFoundRoute;
