import React, {Component} from 'react';
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import {Route} from "react-router";
import {ThemeProvider} from "styled-components";
import theme from "../theme";
import history from "../redux/history";
import Routes from "./routes";
import {configureStore} from "../redux/configureStore";
import {CenterView} from "./styled/views/CenterView";
import logo from "../images/logo.png";

class App extends Component {
  store = null;
  state = {
    ready: false
  };

  componentDidMount() {
    // wait until app state dehydrate from local storage
    this.store = configureStore(() => this.setState({ready: true}));
  }

  render() {
    const {ready} = this.state;
    return (
      <ThemeProvider theme={theme}>
        {ready ? (
          <Provider store={this.store}>
            <ConnectedRouter history={history}
                             onLocationChanged={() => console.log("location changed")}>
              <Route path="/" component={Routes}/>
            </ConnectedRouter>
          </Provider>
        ) : (
          <CenterView>
            <img src={logo} alt="loading"/>
          </CenterView>
        )}
      </ThemeProvider>
    );
  }
}

export default App;
