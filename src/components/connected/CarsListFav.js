import React, {Component} from "react";
import {connect} from "react-redux";
import {VerticalView} from "../styled/views/VerticalView";
import {PaddingView} from "../styled/views/PaddingView";
import {CarListItem} from "./CarsListItem";
import {clearFav, removeFromFav} from "../../redux/carFav/actions";
import {Button} from "../styled/elements/Button";
import {Subtitle} from "../styled/text/Subtitle";

class CarsListFav extends Component {

  removeFromFav = ({stockNumber}) => {
    const {dispatch} = this.props;
    dispatch(removeFromFav({stockNumber}));
  };

  clearFav = () => {
    const {dispatch} = this.props;
    dispatch(clearFav());
  };

  render() {
    const {fav} = this.props;
    return fav && fav.size ? (
      <VerticalView>
        <PaddingView alignSelf="flex-end" right="M" bottom="L">
          <Button primary={true} onClick={() => this.clearFav()}>
            Remove All
          </Button>
        </PaddingView>
        {fav ? fav.map((car, index) => (
          <PaddingView top={index > 0 ? "S" : null} bottom="S" key={car.stockNumber}>
            <CarListItem car={car} onRemove={stockNumber => this.removeFromFav({stockNumber})}/>
          </PaddingView>
        )) : null}
      </VerticalView>
    ) : (
      <VerticalView>
        <Subtitle textAlign="center">
          You have no cars in your favourites list.
        </Subtitle>
      </VerticalView>
    )
  }
}

export default connect((({carFav: {fav}}) => ({
  fav
})))(CarsListFav);
