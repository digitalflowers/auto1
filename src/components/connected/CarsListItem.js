import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import {Link as RouterLink} from 'react-router-dom';
import {VerticalView} from "../styled/views/VerticalView";
import {PaddingView} from "../styled/views/PaddingView";
import {HorizontalView} from "../styled/views/HorizontalView";
import {H2} from "../styled/text/H";
import {ColorMark} from "../styled/others/ColorMark";
import {Link} from "../styled/elements/Link";
import {Button} from "../styled/elements/Button";

const WrapperView = styled.div`
  .car-image {
    // background-color: ${props => props.theme.COLOR_GRAY};
  }
`;

/**
 * car list item
 * @return {*}
 * @constructor
 * @param car
 * @param onRemove
 */
export const CarListItem = ({car: {stockNumber, manufacturerName, modelName, pictureUrl, fuelType, color, mileage}, onRemove}) => (
  <WrapperView>
    <HorizontalView box="M">
      <PaddingView right="L">
        <img src={pictureUrl} width={82} alt={`${manufacturerName} ${modelName}`} className="car-image"/>
      </PaddingView>
      <VerticalView flex={1}>
        <PaddingView bottom="S">
          <H2>{manufacturerName} {modelName}</H2>
        </PaddingView>
        <PaddingView bottom="S">
          Stock # {stockNumber} - {mileage.number} {mileage.unit.toUpperCase()} - {fuelType} - <ColorMark
          color={color}/> {color}
        </PaddingView>
        <Link as={RouterLink} to={`/car/${stockNumber}`}>
          View details
        </Link>
      </VerticalView>
      {onRemove ? (
        <PaddingView alignSelf="center">
          <Button primary={true} onClick={() => onRemove(stockNumber)}>
            Remove
          </Button>
        </PaddingView>
      ) : null}
    </HorizontalView>
  </WrapperView>
);

CarListItem.propTypes = {
  car: PropTypes.shape({
    stockNumber: PropTypes.number,
    manufacturerName: PropTypes.string,
    modelName: PropTypes.string,
    pictureUrl: PropTypes.string,
    fuelType: PropTypes.string,
    color: PropTypes.string,
    mileage: PropTypes.shape({
      number: PropTypes.number,
      unit: PropTypes.string
    })
  }),
  onRemove: PropTypes.func
};
