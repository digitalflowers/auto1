import React, {Component} from "react";
import {connect} from "react-redux";
import {Dropdown} from "../styled/elements/Dropdown";
import {VerticalView} from "../styled/views/VerticalView";
import {PaddingView} from "../styled/views/PaddingView";
import {Preloader} from "../styled/others/Preloader";
import {HorizontalView} from "../styled/views/HorizontalView";
import {Subtitle} from "../styled/text/Subtitle";
import {H2} from "../styled/text/H";
import {Button} from "../styled/elements/Button";
import {Error} from "../styled/text/Error";
import {fetchCars} from "../../redux/cars/actions";
import {CarListItem} from "./CarsListItem";
import {Pager} from "../styled/others/Pager";

class CarsList extends Component {
  componentDidMount() {
    this.fetchCars();
  }

  fetchCars = (extendFilters = {}) => {
    const {dispatch, filters} = this.props;
    dispatch(fetchCars(filters.merge(extendFilters)));
  };

  render() {
    const {pending, error, cars, filters, totalPageCount} = this.props;
    const {sort, page} = filters;
    // Note: total number of cars doesn't available I assume it's {totalPageCount * cars.size}
    return (
      <VerticalView>
        <PaddingView bottom="M">
          <HorizontalView responsive={true} centerMobile={true}>
            <VerticalView flex={1}>
              <H2 textAlign="left">Available cars</H2>
              <Subtitle>Showing {cars.size} of {totalPageCount * cars.size} results</Subtitle>
            </VerticalView>
            <PaddingView alignSelf="flex-end">
              <Dropdown label={"Sort By"}
                        value={sort || "none"}
                        onChange={value => this.fetchCars({sort: value, page: 1})}
                        items={[{
                          value: "none",
                          text: "None"
                        }, {
                          value: "asc",
                          text: "Mileage - Ascending"
                        }, {
                          value: "desc",
                          text: "Mileage - Descending"
                        }]}/>
            </PaddingView>
          </HorizontalView>
        </PaddingView>
        <VerticalView>
          {
            pending ? (
              <VerticalView flex={3} alignItems="center" justifyContent="center">
                <Preloader/>
              </VerticalView>
            ) : (
              error ? (
                <VerticalView flex={3} alignItems="center">
                  <Error>
                    {error.message}
                  </Error>
                  <Button primary={true} onClick={this.fetchFilters}>
                    Retry
                  </Button>
                </VerticalView>
              ) : (
                cars && cars.size ? (
                  <VerticalView flex={3}>
                    {cars.map((car, index) => (
                      <PaddingView top={index > 0 ? "S" : null} bottom="S" key={`${index}-${car.stockNumber}`}>
                        <CarListItem car={car}/>
                      </PaddingView>
                    ))}
                    <PaddingView top="M">
                      <Pager current={page} total={totalPageCount} onSelect={index => this.fetchCars({page: index})}/>
                    </PaddingView>
                  </VerticalView>
                ) : (
                  <VerticalView>
                    <Subtitle textAlign="center">
                      No Items found
                    </Subtitle>
                  </VerticalView>
                )
              )
            )
          }
        </VerticalView>
      </VerticalView>
    )
  }
}

export default connect((({cars: {cars, filters, totalPageCount, error, pending}}) => ({
  cars,
  filters,
  totalPageCount,
  error,
  pending
})))(CarsList);
