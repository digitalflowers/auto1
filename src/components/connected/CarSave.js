import React, {Component} from "react";
import {connect} from "react-redux";
import {VerticalView} from "../styled/views/VerticalView";
import {PaddingView} from "../styled/views/PaddingView";
import {Button} from "../styled/elements/Button";
import {Text} from "../styled/text/Text";
import {addToFav, removeFromFav} from "../../redux/carFav/actions";

class CarSave extends Component {

  toggleSave = () => {
    const {isFav, car, dispatch} = this.props;
    if (isFav) {
      car && car.stockNumber && dispatch(removeFromFav({stockNumber: car.stockNumber}));
    } else {
      car && dispatch(addToFav({car}));
    }
  };

  render() {
    const {isFav} = this.props;
    return (
      <VerticalView box="L">
        <PaddingView bottom="S">
          <Text>
            If you like this car, click the button and save it in your collection of favourite items.
          </Text>
        </PaddingView>
        <PaddingView top="S" alignSelf="flex-end">
          <Button primary={true} onClick={() => this.toggleSave()}>
            {isFav ? "Remove" : "Save"}
          </Button>
        </PaddingView>
      </VerticalView>
    );
  }
}

export default connect((({car: {car}, carFav: {fav}}) => ({
  isFav: car && fav && fav.find(({stockNumber}) => stockNumber === car.stockNumber),
  car,
})))(CarSave);
