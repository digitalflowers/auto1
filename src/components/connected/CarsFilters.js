import React, {Component} from "react";
import {connect} from "react-redux";
import {Dropdown} from "../styled/elements/Dropdown";
import {VerticalView} from "../styled/views/VerticalView";
import {PaddingView} from "../styled/views/PaddingView";
import {Button} from "../styled/elements/Button";
import {fetchCarsFilters} from "../../redux/carsFilters/actions";
import {Preloader} from "../styled/others/Preloader";
import {Error} from "../styled/text/Error";
import {ColorMark} from "../styled/others/ColorMark";
import {fetchCars} from "../../redux/cars/actions";

class CarsFilters extends Component {
  state = {
    color: null,
    manufacturer: null
  };

  componentDidMount() {
    this.fetchFilters();
  }

  fetchFilters = () => {
    const {dispatch} = this.props;
    dispatch(fetchCarsFilters());
  };

  fetchCars = (extendFilters = {}) => {
    const {dispatch, filters} = this.props;
    dispatch(fetchCars(filters.merge(extendFilters)));
  };

  render() {
    const {pending, error, colors, manufacturers, filters} = this.props;
    const {color, manufacturer} = filters;
    return pending ? (
      <VerticalView box="L" alignItems="center" justifyContent="center">
        <Preloader/>
      </VerticalView>
    ) : (
      error ? (
        <VerticalView box="L" alignItems="center">
          <Error>
            {error.message}
          </Error>
          <Button primary={true} onClick={this.fetchFilters}>
            Retry
          </Button>
        </VerticalView>
      ) : (
        <VerticalView box="L">
          <PaddingView bottom="S">
            <Dropdown label={"Color"}
                      value={color || "none"}
                      onChange={value => this.setState({color: value})}
                      items={[{
                        text: "All car colors",
                        value: "none"
                      }].concat(colors
                        .map(color => ({
                          text: (
                            <span>
                              <ColorMark color={color}/> {color}
                            </span>
                          ),
                          value: color
                        }))
                        .toArray()
                      )}/>
          </PaddingView>
          <PaddingView bottom="S">
            <Dropdown label={"Manufacturer"}
                      value={manufacturer || "none"}
                      onChange={value => this.setState({manufacturer: value})}
                      items={[{
                        text: "All manufacturers",
                        value: "none"
                      }].concat(manufacturers
                        .map(({name}) => ({
                          text: name,
                          value: name
                        }))
                        .toArray()
                      )}/>
          </PaddingView>
          <PaddingView top="S" alignSelf="flex-end">
            <Button primary={true} onClick={() => this.fetchCars({
              manufacturer: this.state.manufacturer || manufacturer || "none",
              color: this.state.color || color || "none",
              page: 1
            })}>
              Save
            </Button>
          </PaddingView>
        </VerticalView>
      )
    );
  }
}

export default connect((({carsFilters: {colors, manufacturers, error, pending}, cars: {filters}}) => ({
  colors,
  manufacturers,
  error,
  pending,
  filters
})))(CarsFilters);
