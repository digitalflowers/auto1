const express = require("express");
const routes = require("./routes");

const app = express();
const port = 3001;

// app.use((req, res, next) => {
//   res.header("Access-Control-Allow-Origin", "http://localhost:3000");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

app.use(express.static(__dirname + "/public"));

routes(app);


app.listen(port, () => console.log(`App listening on http://localhost:${port}!`));
